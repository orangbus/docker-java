---
title: docker for java 
---

使用的别名
```bash
alias dc='docker-compose'
alias dcd="docker-compose down"
```
对应的服务相关配置请查看`.env` 文件

# mysql
```bash
dc up -d mysql
```
# redis
```bash
dc up -d redis
```
# mongo
```bash
dc up -d mongo
```
# swagger
```bash
dc up -d swagger
```
# rabbitmq
```bash
dc up -d rabbitmq
```

Ps：
> ## 1、修改了`.env` 配置,重启没有生效
执行：`dcd` 然后重启启动：`dc up -d mysql`

之前的数据会不会丢失？

不会，数据都是进行持久化存储的，具体可查看`data` 文件夹，除非手动删除。

换句话说：在默认的配置情况下，即时你的docker都删除了，只要data文件夹里面的数据还在，即时重新换了一台服务器部署，只要把`data` 文件复制过去覆盖，重新启动，数据依然是存在的，这就是用docker的好处。